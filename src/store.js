import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    avatars: [
      { name: 'Male', isHover: false },
      { name: 'Female', isHover: false }
    ],
    stories: [
      {},
      {
        id: 1,
        name: 'Spaceships',
        audio: 'narration-one.mp3',
        text: 'We had always thought there would be others among the stars...'
      },
      {
        id: 2,
        name: 'Spaceships',
        text: 'We just hadn\'t realised there would be quite so many.',
        audio: 'narration-two.mp3',
        hasSpaceShips: true,
        spaceships: [
          {
            id: 1,
            name: 'spaceship-one',
            animate: 'animated shake',
            image: 'spaceship-one.png'
          },
          {
            id: 2,
            name: 'spaceship-two',
            animate: 'animated shake',
            image: 'spaceship-two.png'
          },
          {
            id: 3,
            name: 'spaceship-three',
            animate: 'animated shake',
            image: 'spaceship-three.png'
          },
          {
            id: 4,
            name: 'spaceship-four',
            animate: 'animated shake',
            image: 'spaceship-four.png'
          }
        ]
      },
      {
        id: 3,
        name: 'Five Races',
        hasGender: true,
        audio: 'narration-three.mp3',
        text: 'We met the scientist Eruditians, the mysterious Cephalords, the warrior Igni, and the builder Formicids. Together, we formed the Galactic Union of Free Stars.'
      },
      {
        id: 4,
        name: 'Void Appearance',
        audio: 'narration-four.mp3',
        text: 'Misunderstandings occurred, but there existed an uneasy peace.' + '\n' + 'This changed with the appearance of The Void.'
      },
      {
        id: 5,
        name: 'Five Cephalord',
        image: 'cephalord-solo.png',
        hasGender: true,
        audio: 'narration-five.mp3',
        text: 'We lost all contact with our allies, the Cephalords.' + '\n' + 'From five, we were down to four.'
      },
      {
        id: 6,
        name: 'No Agreement',
        hasGender: true,
        audio: 'narration-six.mp3',
        text: 'The Galactic Union could not agree on a course of action.' + '\n' + 'In the meantime, The Void continues to grow.'
      },
      {
        id: 7,
        name: 'Voice',
        audio: 'narration-seven.mp3',
        text: 'As the Human ambassador, can you convince the others to unite?' + '\n' + 'Can you be... the Voice Against The Void?'
      }
    ],
    tutorials: [
      {
        id: 1,
        icon: 'purpose.svg',
        name: 'Purpose',
        audio: 'senior-diplomat-2.mp3',
        messages: [
          {
            id: 1,
            text: 'Why we are speaking or writing'
          },
          {
            id: 2,
            text: 'To convince each alien faction to work together to overcome the threat presented by The Void'
          },
          {
            id: 3,
            text: 'Purpose refers to why we are speaking or writing. The Purpose of these talks is to convince each alien faction to work together to overcome the threat presented by The Void. ' +
            'The Galactic Union has not been able to agree on what to do - we hope to persuade them by speaking with each faction individually.'
          }
        ]
      },
      {
        id: 2,
        name: 'Audience',
        icon: 'audience.svg',
        audio: 'senior-diplomat-3.mp3',
        messages: [
          {
            id: 1,
            text: 'Who we are speaking or writing to'
          },
          {
            id: 2,
            text: 'High-ranking leader of each alien faction'
          },
          {
            id: 3,
            text: 'Audience refers to who we are speaking or writing to. The Audience of the talks is a high-ranking leader of each alien faction. ' +
            'Keep this in mind and always be respectful when speaking with them.'
          }
        ]
      },
      {
        id: 3,
        name: 'Context',
        icon: 'context.svg',
        audio: 'senior-diplomat-4.mp3',
        messages: [
          {
            id: 1,
            text: 'Where and on what occasion we are speaking or writing'
          },
          {
            id: 2,
            text: 'Representing the Human Federation'
          },
          {
            id: 3,
            text: 'Context refers to where and on what occasion we are speaking or writing. The Context of the talks is that you will be speaking to the faction leaders while representing the Human Federation. ' +
            'Anything you say would thus reflect the opinion of not just yourself, but the entire Federation.'
          }
        ]
      },
      {
        id: 4,
        name: 'Culture',
        icon: 'culture.svg',
        audio: 'senior-diplomat-5.mp3',
        messages: [
          {
            id: 1,
            text: 'Values, beliefs and customs of the person or group we are speaking or writing to'
          },
          {
            id: 2,
            text: 'Differs for each faction - find out more using the Galactic Map'
          },
          {
            id: 3,
            text: 'Culture refers to the values, beliefs and customs of the person or group we are speaking or writing to. The Culture for each talk will differ depending on which alien faction you are speaking with. ' +
            'You can find out more about the Culture of each faction by selecting the relevant section on the Galactic Map.'
          }
        ]
      }
    ],
    factions: [
      {
        id: 1,
        planet: 'human',
        soloImage: 'male-solo.png',
        race: 'Human Federation',
        audio: 'ai-earth-map.mp3',
        description: 'Ambassador, this territory belongs to our faction. The Human Federation is the newest member of the Galactic Union and is not looked upon as a serious competitor by the other factions. Since joining the Union, the Human Federation has worked hard to build good relations with other factions by negotiating trade deals and collaborating on mutually beneficial projects.'
      },
      {
        id: 2,
        planet: 'igni',
        soloImage: 'igni-solo.png',
        required: true,
        name: 'Igni General',
        gender: 'Male',
        race: 'Igni Empire',
        audio: 'ai-igni-map.mp3',
        style: 'is-light',
        images: [
          {
            id: 1,
            title: 'neutral',
            image: 'igni-neutral.png'
          },
          {
            id: 2,
            title: 'happy',
            image: 'igni-happy.png'
          },
          {
            id: 3,
            title: 'angry',
            image: 'igni-angry.png'
          }
        ],
        background: 'igni-bg.jpg',
        goodbye: 'The Igni Empire shall send its ships to help in the border worlds. Good day, Ambassador.',
        description: 'This territory belongs to the Igni Empire. The Igni are a proud people with a long history as mighty warriors. They are fiercely competitive and cannot back down from a challenge. The Empire maintains a mighty space armada, and the Igni view themselves as the protectors of the galaxy.',
        hint: 'Ambassador - which statement do you think will appeal to the Igni? Keep in mind their competitive culture.',
        audioHint: 'ai-igni-help.mp3',
        quiz: [
          {
            id: 1,
            question: 'Ah, Ambassador. The Igni Empire recognises the strength of the Human Federation. What do you want today?',
            questionRepeat: 'What else do you want, Human? I haven\'t got all day.',
            choices: [
              {
                id: 1,
                text: 'We must reinforce our borders to defend against The Void.',
                correct: true,
                score: 1,
                reaction: 'The Igni General seems pleased with your suggestion. (+1)',
                response: 'The Igni are the natural protectors of our galaxy. Indeed, our mighty Igni warships stand ready to defend our Empire.'
              },
              {
                id: 2,
                text: 'Let us retreat from the border worlds for the safety of our people!',
                score: -2,
                reaction: 'The Igni General seems insulted by your suggestion. (-2)',
                response: 'Shall we run like cowards, then? This may suit the Humans, but not the brave Igni!'
              },
              {
                id: 3,
                text: 'Should we study The Void in order to find out more about its nature?',
                score: -1,
                reaction: 'The Igni General seems annoyed by your suggestion. (-1)',
                response: 'There is a time for thinking, and there is a time for action. For the Igni, it is always time for action.'
              },
              {
                id: 4,
                text: 'With Human leadership and your support, we can defeat The Void!',
                score: 0,
                reaction: 'The Igni General seems unimpressed by your suggestion. (0)',
                response: 'Ha. The Humans have always been bold, and we respect that. But... no. The Igni should lead, not you.'
              }
            ]
          },
          {
            id: 2,
            question: 'But tell us, why should we aid the Galactic Union?',
            questionRepeat: 'Why else, Human, should we aid the Galactic Union?',
            choices: [
              {
                id: 1,
                text: 'We need to be united in our action in this time of crisis.',
                score: 0,
                reaction: 'The Igni General seems unconvinced by your reason. (0)',
                response: 'Of course. Would you be so kind as to tell the Eruditians and Formicids to unite behind the Igni Empire, then?'
              },
              {
                id: 2,
                text: 'It is for the common good!',
                score: -1,
                reaction: 'The Igni General seems unimpressed by your reason. (-1)',
                response: 'Ha! What is "the common good" to the great Igni Empire? Let the strong survive; the weak shall perish!'
              },
              {
                id: 3,
                text: 'Show us the might of your great civilisation!',
                correct: true,
                score: 1,
                reaction: 'The Igni General seems convinced by your reason. (+1)',
                response: 'It is good that you acknowledge our superior strength. We shall help to defend our borders against The Void - just remember who your saviours are!'
              },
              {
                id: 4,
                text: 'We should not rush into action without more planning.',
                score: 0,
                reaction: 'The Igni General seems unimpressed with your reason. (0)',
                response: 'There is a time for thinking, and there is a time for action. For the Igni, it is always time for action.'
              }
            ]
          }
        ]
      },
      {
        id: 3,
        planet: 'eruditian',
        soloImage: 'eruditian-solo.png',
        required: true,
        name: 'Eruditian Officer',
        gender: 'Female',
        race: 'Eruditian Enclave',
        audio: 'ai-eruditian-map.mp3',
        images: [
          {
            id: 1,
            title: 'neutral',
            image: 'eruditian-neutral.png'
          },
          {
            id: 2,
            title: 'happy',
            image: 'eruditian-happy.png'
          },
          {
            id: 3,
            title: 'angry',
            image: 'eruditian-angry.png'
          }
        ],
        background: 'eruditian-bg.jpg',
        goodbye: 'The Eruditian Enclave shall research The Void and share its findings with the Union. Good day, Ambassador.',
        description: 'This is the territory of the Eruditian Enclave. The Eruditians value knowledge above all else, and their society is the most scientifically advanced in the galaxy. As a people, they prefer to avoid interaction; however, our Research Treaty with them shows that they can be persuaded to negotiate.',
        hint: 'Ambassador - which statement do you think will appeal to the Eruditians? Keep in mind their knowledge-seeking culture.',
        audioHint: 'ai-eruditian-help.mp3',
        quiz: [
          {
            id: 1,
            question: 'Greetings, Ambassador. The Eruditian Enclave sees and knows the Human Federation. What thoughts do you have to share today?',
            questionRepeat: 'What else is on your mind, Human?',
            choices: [
              {
                id: 1,
                text: 'We must reinforce our borders to defend against The Void.',
                score: -1,
                reaction: 'The Eruditian Officer seems annoyed by your suggestion. (-1)',
                response: 'And how do you propose we succeed where the Cephalords have failed? No, victory must come from another direction.'
              },
              {
                id: 2,
                text: 'Let us retreat from the border worlds for the safety of our people!',
                score: 0,
                reaction: 'The Eruditian Officer seems unimpressed by your suggestion. (0)',
                response: 'We can try to run, but The Void continues to grow. This is no solution.'
              },
              {
                id: 3,
                text: 'Should we study The Void in order to find out more about its nature?',
                correct: true,
                score: 1,
                reaction: 'The Eruditian Officer seems pleased with your suggestion. (+1)',
                response: 'Yes, knowledge is power. We must understand The Void in order to overcome it.'
              },
              {
                id: 4,
                text: 'With Human leadership and your support, we can defeat The Void!',
                score: -2,
                reaction: 'The Eruditian Officer seems insulted by your suggestion. (-2)',
                response: 'Do you believe your people think better than the Eruditians? No, we cannot abandon the pursuit of knowledge at this point in time.'
              }
            ]
          },
          {
            id: 2,
            question: 'But tell us, why should we aid the Galactic Union?',
            questionRepeat: 'Why else, Human, should we aid the Galactic Union?',
            choices: [
              {
                id: 1,
                text: 'We need to be united in our action in this time of crisis.',
                score: -1,
                reaction: 'The Eruditian Officer seems unimpressed with your reason. (-1)',
                response: 'Unity is meaningless if we are united behind a foolish cause.'
              },
              {
                id: 2,
                text: 'It is for the common good!',
                score: 0,
                reaction: 'The Eruditian Officer seems unconvinced by your reason. (0)',
                response: 'We too wish for the common good. But how do we know what course of action to take to achieve it?'
              },
              {
                id: 3,
                text: 'Show us the might of your great civilisation!',
                score: -1,
                reaction: 'The Eruditian Officer seems unimpressed with your reason. (-1)',
                response: 'We already know of our civilisation\'s might. There is no need to show it off.'
              },
              {
                id: 4,
                text: 'We should not rush into action without more planning.',
                correct: true,
                score: 1,
                reaction: 'The Eruditian Officer seems convinced by your reason. (+1)',
                response: 'It is wise to look before we leap. Yes, the Galactic Union will benefit from our research on the matter.'
              }
            ]
          }
        ]
      },
      {
        id: 4,
        planet: 'formicid',
        soloImage: 'formicid-solo.png',
        required: true,
        name: 'Formicid Envoy',
        gender: 'Male',
        race: 'Formicid Hive',
        audio: 'ai-formicid-map.mp3',
        images: [
          {
            id: 1,
            title: 'neutral',
            image: 'formicid-neutral.png'
          },
          {
            id: 2,
            title: 'happy',
            image: 'formicid-happy.png'
          },
          {
            id: 3,
            title: 'angry',
            image: 'formicid-angry.png'
          }
        ],
        background: 'formicid-bg.jpg',
        goodbye: 'The Formicid Hive shall build the necessary ships and structures to evacuate our border worlds. Good day, Ambassador.',
        description: 'The Formicid Hive occupies this territory. A founding member of the Galactic Union, the Formicids are known for their feats of engineering. With their gift of efficient quality construction, these master builders have been able to spread quickly throughout the galaxy. The Formicid society is a selfless one – each Formicid thinks of the Hive before self.',
        hint: 'Ambassador - which statement do you think will appeal to the Formicids? Keep in mind their culture of selflessness.',
        audioHint: 'ai-formicid-help.mp3',
        quiz: [
          {
            id: 1,
            question: 'Greetings, Ambassador. The Formicid Hive welcomes the representative of the Human Federation. How might we help today?',
            questionRepeat: 'How else might we help, Human?',
            choices: [
              {
                id: 1,
                text: 'We must reinforce our borders to defend against The Void.',
                score: 0,
                reaction: 'The Formicid Envoy seems unimpressed by your suggestion. (0)',
                response: 'We can try to do so, but the Formicids are not warriors. We will not stop The Void for long.'
              },
              {
                id: 2,
                text: 'Let us retreat from the border worlds for the safety of our people!',
                correct: true,
                score: 1,
                reaction: 'The Formicid Envoy seems pleased with your suggestion. (+1)',
                response: 'The Hive desires the safety of all peoples. It is good that you wish to rescue the citizens of the border worlds.'
              },
              {
                id: 3,
                text: 'Should we study The Void in order to find out more about its nature?',
                score: -1,
                reaction: 'The Formicid Envoy seems annoyed by your suggestion. (-1)',
                response: 'Do you also propose letting more worlds be lost while we conduct this research? No, the time for study is past.'
              },
              {
                id: 4,
                text: 'With Human leadership and your support, we can defeat The Void!',
                score: -2,
                reaction: 'The Formicid Envoy seems insulted by your suggestion. (-2)',
                response: 'Is any one faction more important than the others? No, we must consider the needs of all factions in the decisions of the Galactic Union.'
              }
            ]
          },
          {
            id: 2,
            question: 'But tell us, why should we aid the Galactic Union?',
            questionRepeat: 'Why else, Human, should we aid the Galactic Union?',
            choices: [
              {
                id: 1,
                text: 'We need to be united in our action in this time of crisis.',
                score: 0,
                reaction: 'The Formicid Envoy seems unconvinced by your reason. (0)',
                response: 'Unity is admirable. But for what cause shall we unite ourselves?'
              },
              {
                id: 2,
                text: 'It is for the common good!',
                correct: true,
                score: +1,
                reaction: 'The Formicid Envoy seems convinced by your reason. (+1)',
                response: 'Yes, we must choose the course of action that best supports the Galactic Union. Saving our border worlds helps us all.'
              },
              {
                id: 3,
                text: 'Show us the might of your great civilisation!',
                score: -1,
                reaction: 'The Formicid Envoy seems unimpressed with your reason. (-1)',
                response: 'What purpose does it serve to showcase the might of any one faction?'
              },
              {
                id: 4,
                text: 'We should not rush into action without more planning.',
                score: -1,
                reaction: 'The Formicid Envoy seems unimpressed with your reason. (-1)',
                response: 'Every moment spent planning is another moment The Void grows. How long before it devours us all?'
              }
            ]
          }
        ]
      },
      {
        id: 5,
        planet: 'cephalord',
        soloImage: 'cephalord-solo.png',
        race: 'Cephalord Domain',
        audio: 'ai-cephalord-map.mp3',
        description: 'The Cephalord Domain’s territory has been taken over by The Void. A founding member of the Galactic Union, the Cephalords have been uncontactable since the appearance of The Void. Some believe the entire faction to have been lost.'
      }
    ],
    selectedAvatar: null,
    currentFactionIndex: null,
    currentFaction: false,
    arraySelectedFaction: [],
    totalScore: 0,
    correctScreen: false,
    conclusions: [
      {
        id: 1,
        result: 'bad',
        title: 'Disastrous Ending',
        audio: 'ai-ending-bad.mp3',
        text: 'The factions were not convinced by your responses. Keep their culture in mind when speaking with them and see if you can do better and unlock the other two possible endings!'
      },
      {
        id: 2,
        result: 'neutral',
        title: 'Neutral Ending',
        audio: 'ai-ending-neutral.mp3',
        text: 'The factions were only somewhat convinced by your responses. Keep their culture in mind when speaking with them and see if you can do better to unlock the best of three possible endings!'
      },
      {
        id: 3,
        result: 'good',
        title: 'Perfect Ending',
        audio: 'ai-ending-good.mp3',
        text: 'The factions were fully convinced by your responses. You have done well by keeping their culture in mind when speaking with them and have unlocked the best of three possible endings!'
      }
    ]
  },
  mutations: {
    correctScreen (state, status) {
      state.correctScreen = status
    },
    setAvatar (state, avatar) {
      state.selectedAvatar = avatar
    },
    toggleAvatarHover (state, avatar) {
      const index = state.avatars.indexOf(avatar)

      state.avatars[index].isHover = !state.avatars[index].isHover
    },
    setCurrentFactionIndex (state, index) {
      state.currentFactionIndex = index
    },
    setCurrentFaction (state, status) {
      state.currentFaction = status
    },
    appendSelectedFaction (state, id) {
      state.arraySelectedFaction.push(id)
    },
    calculateScore (state, score) {
      state.totalScore += score
    }
  },
  getters: {
    faction (state) {
      return state.factions[state.currentFactionIndex]
    },
    isCompleted (state) {
      let required = []
      let metRequirement = []

      state.factions.forEach(faction => {
        if (faction.required) {
          required.push(faction.id)
        }
      })

      required.forEach(requirement => {
        metRequirement.push(state.arraySelectedFaction.includes(requirement))
      })

      return state.arraySelectedFaction.length > 0 && metRequirement.every(requirement => requirement === true)
    },
    conclusion (state) {
      const score = state.totalScore
      let conclusion = []

      if (score >= 6) {
        conclusion = state.conclusions[2]
      } else if (score > 0) {
        conclusion = state.conclusions[1]
      } else if (score <= 0) {
        conclusion = state.conclusions[0]
      }

      return conclusion
    }
  }
})
