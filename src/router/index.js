import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import SplashScreen from '@/components/SplashScreen'
import ChooseAvatar from '@/components/ChooseAvatar'
import Introduction from '@/components/Introduction'
import Tutorial from '@/components/Tutorial'
import Activity from '@/components/Activity'
import Quiz from '@/components/Quiz'
import Conclusion from '@/components/Conclusion'
import Credits from '@/components/Credits'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright,
      meta: {
        title: 'Copyright'
      }
    },
    {
      path: '/splash-screen',
      name: 'SplashScreen',
      component: SplashScreen,
      meta: {
        title: 'Splash Screen'
      }
    },
    {
      path: '/choose-avatar',
      name: 'ChooseAvatar',
      component: ChooseAvatar,
      meta: {
        title: 'Choose Avatar'
      }
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/tutorial',
      name: 'Tutorial',
      component: Tutorial,
      meta: {
        title: 'Tutorial'
      }
    },
    {
      path: '/activity',
      name: 'Activity',
      component: Activity,
      meta: {
        title: 'Activity'
      }
    },
    {
      path: '/quiz',
      name: 'Quiz',
      component: Quiz,
      meta: {
        title: 'Quiz'
      }
    },
    {
      path: '/conclusion',
      name: 'Conclusion',
      component: Conclusion,
      meta: {
        title: 'Conclusion'
      }
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits,
      meta: {
        title: 'Credits'
      }
    }
  ]
})
