# Changelog

## Unreleased
- Minor text fixes

## v2.0.8
- Fixed broken Activity component due to the wrong filename

## v2.0.7
- Added badges to README
- Added copyright video by MOE CPDD

## v2.0.6
- Fixed transitions flashing

## v2.0.5
- Fixed an issue where "Skip Tutorial" button is showing when it is not supposed to

## v2.0.4
- Optimised webpack configuration

## v2.0.3
- Removed Font Awesome since it is not being used for optimisation purpose

## v2.0.2
- Replaced `.push()` for `.replace()` to remove links from history
- Modified additional styling
- Removed e2e runner stuff
- Modified routes to use ESM imports
- Fixed site not being able to open locally via `index.html`

## v2.0.1
- Upgraded Font Awesome to v5
- Fixed broken refactor 

## v2.0.0
- Upgrade to Webpack v4

## v1.1.0
- Cleanup and refactor components

## v1.0.0
Initial release

- Added splash screen
- Fixed bug where quiz will terminate prematurely
- Remove audio temporary
- Added function to disable wrong choices after selected in Quiz 
- Fixed an issue with "Human" and "Void" not able to proceed further
- Fixed an issue with conclusion page jumping back
