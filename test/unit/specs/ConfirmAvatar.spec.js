import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import ConfirmAvatar from '@/components/ConfirmAvatar'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('ConfirmAvatar', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      setAvatar: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(ConfirmAvatar, {
      store,
      localVue,
      propsData: {
        isActive: true,
        selectedAvatar: { name: 'Male' }
      }
    })
  })

  test('', () => {
    wrapper.setMethods({ closeModal: jest.fn() })

    wrapper.find('.is-yes').trigger('click')

    expect(mutations.setAvatar).toHaveBeenCalled()

    wrapper.vm.$emit('incrementStoryCounter')

    expect(wrapper.emitted().incrementStoryCounter).toBeTruthy()
    expect(wrapper.vm.closeModal).toBeTruthy()
  })

  test('should close modal when button is clicked', () => {
    wrapper.find('.is-no').trigger('click')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should play sound effect when mouseover', () => {
    wrapper.setMethods({ playAudio: jest.fn() })

    wrapper.findAll('button').at(0).trigger('mouseover')

    expect(wrapper.vm.playAudio).toBeTruthy()
  })
})
