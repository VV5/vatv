import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Conclusion from '@/components/Conclusion'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Conclusion', () => {
  let wrapper, computed

  beforeEach(() => {
    computed = {
      video: jest.fn()
    }

    wrapper = shallowMount(Conclusion, {
      store, localVue, computed
    })
  })

  test('defaults total score to zero', () => {
    expect(wrapper.vm.totalScore).toBe(0)
  })

  test('video should trigger ended event', () => {
    wrapper.find('video').trigger('ended')

    expect(wrapper.vm.videoEnded).toBe(true)
  })

// test('should render good consequence video if score is greater than zero', () => {
//   state = {
//     conclusions: [],
//     totalScore: 1
//   }
//
//   store = new Vuex.Store({
//     state,
//     getters
//   })
//
//   wrapper = shallowMount(Conclusion, {
//     store,
//     localVue,
//     computed
//   })
//
//   expect(wrapper.vm.conclusion).toBe('bad')
// })
})
