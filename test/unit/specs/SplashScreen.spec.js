import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import SplashScreen from '@/components/SplashScreen'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('SplashScreen', () => {
  let wrapper, playAudioStub

  beforeEach(() => {
    wrapper = shallowMount(SplashScreen)
  })

  test('should play sound effect when hover over any buttons', () => {
    playAudioStub = sinon.stub()
    wrapper.setMethods({ playAudio: playAudioStub })

    wrapper.findAll('button').at(0).trigger('mouseover')

    expect(playAudioStub.called).toBe(true)
  })

  test('should navigate to choose avatar page when button is clicked', () => {
    const $route = {
      path: '/choose-avatar'
    }

    const router = new VueRouter()

    wrapper = shallowMount(SplashScreen, {
      router, localVue
    })

    playAudioStub = sinon.stub()
    wrapper.setMethods({ playAudio: playAudioStub })

    wrapper.find('button').trigger('click')

    expect(playAudioStub.called).toBe(true)
    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
