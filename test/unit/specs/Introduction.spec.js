import { mount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {}

    store = new Vuex.Store({
      state
    })

    wrapper = mount(Introduction, {
      store, localVue
    })

    window.HTMLAudioElement.prototype.load = () => {}
    window.HTMLAudioElement.prototype.play = () => {}
  })

  test('defaults first try to true', () => {
    expect(wrapper.vm.isFirstTry).toBe(true)
  })

  test('should play sound effect when mouse over any buttons', () => {
    const playAudioStub = sinon.stub()

    wrapper.setMethods({ playAudio: playAudioStub })

    wrapper.findAll('button').at(0).trigger('mouseover')

    expect(playAudioStub.called).toBe(true)
  })

  test('should open modal dialog when skip button is clicked', () => {
    wrapper.setData({
      isFirstTry: false
    })

    wrapper.find('.is-skip').trigger('click')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should navigate to tutorial when view button is clicked', () => {
    const $route = {
      path: '/tutorial'
    }

    const router = new VueRouter()

    const wrapper = mount(Introduction, {
      store, router, localVue
    })

    wrapper.setData({
      isFirstTry: false
    })

    wrapper.find('.is-view').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should close modal dialog', () => {
    wrapper.find('.is-no').trigger('click')

    expect(wrapper.vm.isActive).toBe(false)
  })
})
