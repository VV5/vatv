import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import ConfirmModal from '@/components/ConfirmModal'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('ConfirmModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ConfirmModal, {
      propsData: {
        isActive: true
      }
    })
  })

  test('should play sound effect when hover over any buttons', () => {
    const playAudioStub = sinon.stub()

    wrapper.setMethods({playAudio: playAudioStub})

    wrapper.findAll('button').at(0).trigger('mouseover')

    expect(playAudioStub.called).toBe(true)
  })

  test('should navigate to activity when yes button is clicked', () => {
    const router = new VueRouter()

    const wrapper = shallowMount(ConfirmModal, {
      router, localVue
    })

    wrapper.find('.is-yes').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/activity')
  })

  test('should close dialog when no button is clicked', () => {
    wrapper.find('.is-no').trigger('click')

    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
