import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Tutorial from '@/components/Tutorial'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Tutorial.vue', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {
      tutorials: [
        { id: 1 }, { id: 2 }
      ]
    }

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Tutorial, {
      attachToDocument: true,
      store,
      localVue
    })
  })

  test('should close modal when ESC button is pressed', () => {
    wrapper.setData({ isActive: true })

    wrapper.trigger('keyup', { key: 'Escape' })

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should not close the modal when buttons other than ESC is pressed', () => {
    wrapper.setData({ isActive: true })

    wrapper.trigger('keyup')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should play sound effect when hover over any buttons', () => {
    const playAudioStub = sinon.stub()

    wrapper.setMethods({
      isDisabled: false,
      playAudio: playAudioStub
    })

    wrapper.find('.is-next').trigger('mouseover')

    expect(playAudioStub.called).toBe(true)
  })

  test('should show list of tutorials when image is clicked', () => {
    wrapper.find('.is-tutorial-icon').trigger('click')

    expect(wrapper.vm.showTutorial).toBe(true)
  })

  test('should open modal when tutorial button is clicked', () => {
    wrapper.setData({ showTutorial: true })

    wrapper.findAll('.is-tutorial').at(0).trigger('click')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should navigate to the correct path', () => {
    const $route = {
      path: '/activity'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Tutorial, {
      store, router, localVue
    })

    wrapper.setData({ isDisabled: false })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
