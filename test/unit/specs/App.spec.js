import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from '@/App'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      correctScreen: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(App, {
      attachToDocument: true,
      store,
      localVue
    })
  })

  test('should set the correct screen mode when window is re-sized', () => {
    wrapper.trigger('resize')

    expect(mutations.correctScreen).toHaveBeenCalled()
  })
})
