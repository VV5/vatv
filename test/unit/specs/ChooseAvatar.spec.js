import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import ChooseAvatar from '@/components/ChooseAvatar'
import ConfirmAvatar from '@/components/ConfirmAvatar'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('ChooseAvatar', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {
      avatars: [
        { name: 'Male' }, { name: 'Female' }
      ]
    }

    mutations = {
      setAvatar: jest.fn(),
      toggleAvatarHover: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(ChooseAvatar, {
      store, localVue
    })

    wrapper.setMethods({ playAudio: jest.fn() })
  })

  test('should change image when mouseover and mouseleave', () => {
    wrapper.findAll('.is-avatar').at(0).trigger('mouseover')
    expect(mutations.toggleAvatarHover).toHaveBeenCalled()

    wrapper.findAll('.is-avatar').at(0).trigger('mouseleave')
    expect(mutations.toggleAvatarHover).toHaveBeenCalled()
  })

  test('should open avatar confirmation modal', () => {
    wrapper.find('.is-male').trigger('click')

    expect(wrapper.vm.isActive).toBe(true)
  })
})
