import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Copyright from '@/components/Copyright'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Copyright', () => {
  let wrapper

  test('should navigate to Splash Screen when copyright video has ended', () => {
    const $router = {
      path: '/splash-screen'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Copyright, {
      router, localVue
    })

    wrapper.find('video').trigger('ended')

    expect(wrapper.vm.$route.path).toBe($router.path)
  })
})
